


import sys
import os
import tarfile



def decompress(dir):
	fnames = os.listdir(dir)
	nfiles = len(fnames)
	nextracted = 0
	for fname in fnames:
		if fname.endswith('.tar.gz'):
			tar = tarfile.open(dir+'/'+fname, 'r:gz')
			tar.extractall(dir)
			tar.close()
			nextracted += 1

	print(">>> Extracted {} files out of {} files in folder".format(nextracted, nfiles))

if __name__ == '__main__':
	dir = sys.argv[1]
	decompress(dir)